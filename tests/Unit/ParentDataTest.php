<?php

namespace Tests\Unit;

use App\Models\ParentData;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ParentDataTest extends TestCase
{
	use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
	
	public function test_can_list_parent_data() {
		
		$parents = ParentData::factory()->create();
		
		$this->json('GET','/api/list_all_parents')
			->assertStatus(200)
			->assertJson([$parents->toArray()])
			->assertJsonStructure([
				'*' => ['id','balance', 'currency', 'email','status_code','registration_date','identifier','data_provider','created_at','updated_at'],
			]);
	}
}
