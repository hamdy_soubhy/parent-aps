<?php

namespace App\Http\Controllers;

use App\Models\ParentData;
use App\Models\StatusCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProviderController extends Controller
{
    //
	
	public function provider($provider){
		$files = Storage::files('public/'.$provider);
		foreach ($files as $file){
			$file_arr = explode('/',$file);
			$provider_files[] = $file_arr[2];
		}
		return view('provider.provider_files',compact('provider_files','provider'));
	}
	
	public function import($folder, $filename){
		if (Storage::exists('public/'.$folder.'/'.$filename)){
			
			$file_content_json = json_decode(Storage::get('public/'.$folder.'/'.$filename));
			
			foreach ($file_content_json->users as $user){
				
				$new_user = $this->changeArrayKeys($user);
				
				$status_code = StatusCode::where('status_code',$new_user['status_code'])->first();
				
				$new_user['status_code'] = $status_code->status->status;
				$new_user['registration_date'] = date('Y-m-d',strtotime($new_user['registration_date']));
				$new_user['data_provider'] = $folder;
				
				ParentData::Create($new_user);
				
			}
			// ===========================
			// here i supposed to delete the file after importing it but i kept for testing
			// ===========================
			return redirect()->back()->with('success', 'Data imported successfully!');
			
		}else{
			return redirect()->back()->with('error', 'File does not exist!');
		}
	}
	
	public function changeArrayKeys($user){
		
		$new_keys = array('balance','currency', 'email','status_code','registration_date','identifier');
		$i = 0;
		foreach($user as $key=>$value){
			$new_user[$new_keys[$i]] = $value;
			$i++;
		}
		return $new_user;
	}
}
