<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    //
	public function index(){
		$directories = Storage::directories('public');
		$providers = array();
		foreach ($directories as $directory) {
			$directory_arr = explode('/',$directory);
			$providers[] = $directory_arr[1];
		}
		return view('pages.home', compact('providers'));
	}
}
