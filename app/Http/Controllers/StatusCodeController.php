<?php

namespace App\Http\Controllers;

use App\Models\StatusCode;
use Illuminate\Http\Request;

class StatusCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatusCode  $statusCode
     * @return \Illuminate\Http\Response
     */
    public function show(StatusCode $statusCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StatusCode  $statusCode
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusCode $statusCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StatusCode  $statusCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusCode $statusCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatusCode  $statusCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusCode $statusCode)
    {
        //
    }
}
