<?php

namespace App\Http\Controllers;

use App\Models\ParentData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParentDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	    $params = $request->query();
	    
	    $parents = ParentData::query();
	    
	    if($request->filled('provider'))
	    	$parents->where('data_provider',$params['provider']);
	    
	    if($request->filled('statusCode'))
		    $parents->where('status_code',$params['statusCode']);
	
	    if($request->filled('balanceMin'))
		    $parents->where('balance', '>=' ,$params['balanceMin']);
	
	    if($request->filled('balanceMax'))
		    $parents->where('balance','<=',$params['balanceMax']);
	
	    if($request->filled('currency'))
		    $parents->where('currency',$params['currency']);
	    
	    return $parents->get()->toJson();
	    
    }


}
