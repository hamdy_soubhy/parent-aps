<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParentData extends Model
{
    use HasFactory;
    protected $fillable = ['balance','currency','email','status_code','registration_date','identifier','data_provider'];
    
    
    
}
