<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusCode extends Model
{
    use HasFactory;
    protected $fillable = ['status_code','status_id'];
    
    public function status(){
    	return $this->belongsTo(Status::class,'status_id','id');
    }
}
