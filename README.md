## Installation

- Clone Repository into you root folder or any other folder.
- CD to the App
- Create new empty db
- Create .env file and update it with the created db and credentials.
- Run in terminal 
  ```
        composer install
    ```
- Run in terminal 
  ```
        php artisan key:generate
    ```
- Run in terminal
    ```
        php artisan migrate
    ```
- Run in terminal
    ```
        php artisan db:seed
    ```
- Create providers data folders inside the following dir:
    -App directory-/storage/app/public
    
    ex. parent-aps/storage/app/public/DataProviderX/DataProviderX.json
    
- Run in terminal:
    ```
      php artisan serve
    ```
- Open you browser and insert the provided url after you run the previous command
    
    ex.http://127.0.0.1:8000
         
- Import data through the browser
 
- You can fetch the data through the following api url through any rest client like postman

    http://127.0.0.1:8000/api/list_all_parents

- Run in terminal the following command to run the unit test
    ```
      php artisan test
    ```
## Requirements

- PHP >= 7.3
- Composer