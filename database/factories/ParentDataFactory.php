<?php

namespace Database\Factories;

use App\Models\ParentData;
use App\Models\StatusCode;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ParentDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ParentData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'balance' => $this->faker->numberBetween(200,1000),
	        'currency' => $this->faker->currencyCode,
	        'email' => $this->faker->unique()->safeEmail,
	        'status_code' => $this->faker->randomElement(array('authorised','decline','refunded')),
	        'registration_date' => $this->faker->date('Y-m-d'),
	        'identifier' => Str::random(15),
	        'data_provider' => $this->faker->randomElement(array('DataProviderY','DataProviderX')),
	        
        ];
    }
}
