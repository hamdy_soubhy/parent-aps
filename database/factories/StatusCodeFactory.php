<?php

namespace Database\Factories;

use App\Models\StatusCode;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusCodeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StatusCode::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
