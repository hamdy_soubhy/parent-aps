<?php

namespace Database\Seeders;

use App\Models\Status;
use App\Models\StatusCode;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class StatusCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	    Schema::disableForeignKeyConstraints();
	    
	    Status::truncate();
	    StatusCode::truncate();
	    
	    Schema::enableForeignKeyConstraints();
	    
	    $status = Status::create(['status'=>'authorised']);
	    StatusCode::create(['status_code' =>1 , 'status_id' => $status->id]);
	    StatusCode::create(['status_code' =>100 , 'status_id' => $status->id]);
	    
	    $status = Status::create(['status'=>'decline']);
	    StatusCode::create(['status_code' =>2 , 'status_id' => $status->id]);
	    StatusCode::create(['status_code' =>200 , 'status_id' => $status->id]);
	    
	    $status = Status::create(['status'=>'refunded']);
	    StatusCode::create(['status_code' =>3 , 'status_id' => $status->id]);
	    StatusCode::create(['status_code' =>300 , 'status_id' => $status->id]);
    }
}
