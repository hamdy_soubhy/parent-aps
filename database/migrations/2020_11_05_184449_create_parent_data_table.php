<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParentDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_data', function (Blueprint $table) {
            $table->id();
            $table->integer('balance')->unsigned();
	        $table->string('currency');
	        $table->string('email')->unique();
	        $table->string('status_code');
	        $table->date('registration_date');
	        $table->string('identifier');
	        $table->string('data_provider');
	        
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_data');
    }
}
