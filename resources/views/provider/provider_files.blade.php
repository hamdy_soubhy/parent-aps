@extends('layouts.default')
@section('content')
	<div class="bs-docs-section">

		<div class="row">
			<div class="col-sm-12">

				<div class="bs-component">
					<div class="jumbotron">
						@if (\Session::has('error'))
							<div class="alert alert-danger">
								<ul>
									<li>{!! \Session::get('error') !!}</li>
								</ul>
							</div>
						@endif
						@if (\Session::has('success'))
							<div class="alert alert-success">
								<ul>
									<li>{!! \Session::get('success') !!}</li>
								</ul>
							</div>
						@endif
						<h4 class="">{{$provider}} files:</h4>
						<ul>
							@foreach($provider_files as $file)
								<li>
									<div>
										<h6>{{$file}}</h6>
										<a class="" href="{{url('/provider/import')}}/{{$provider}}/{{$file}}" role="button">Import</a>

									</div>
								</li>
							@endforeach
						</ul>

					</div>
				</div>
			</div>
		</div>


	</div>

@stop