@extends('layouts.default')
@section('content')
	<div class="bs-docs-section">

		<div class="row">
			<div class="col-sm-12">

				<div class="bs-component">
					<div class="jumbotron">
						<h3 class="">Providers</h3>
						@if($providers)
						<ul>
							@foreach($providers as $provider)
								<li><a href="{{url("/provider")}}/{{$provider}}">{{$provider}}</a></li>
							@endforeach
						</ul>
						@else
							<p> No Providers were found </p>
						@endif
						<p class="lead">
						</p>
					</div>
				</div>
			</div>
		</div>


	</div>

@stop